from contact_search import *
from contact_delete import *
from contacts_display_all import *


def main():

    while True:
        user_input = input("Hello!,\n\n select the operation you want to perform:\n\n 1 -> display all contact\n 2 -> Add contact\n 3 -> Delete contact\n 4 -> search contact\n\ntype 'exit' to quit\n")
        if user_input.lower() == "exit":
            break
        user_choice = user_input.split()

        if len(user_choice) == 1:
            if user_choice[0].isdigit() and int(user_choice[0]) == 1:
                show_all_contacts()
                break
            elif user_choice[0].isdigit() and int(user_choice[0]) == 2:
                add_contact()
                break
            elif user_choice[0].isdigit() and int(user_choice[0]) == 3:
                contact_delete()
                break
            elif user_choice[0].isdigit() and int(user_choice[0]) == 4:
                search_contact()
                break
            else:
                print("you are out of range [1-4]")


if __name__ == "__main__":
    main()