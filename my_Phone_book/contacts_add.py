import contacts_manager


def display_registered_contact(contact_saved):
    print(f"the contact {contact_saved} has been saved")


def validate_email(email):
    allowed_characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@.-_"
    return all(char in allowed_characters for char in email)


def add_contact():
    contacts = contacts_manager.load_contacts("contacts.txt")
        
    while True:
        user_info_input = input(
            "add your name, email, phone contact in this order separated as comma, or type 'exit' to quit\n")
        if user_info_input.lower() == "exit":
            break

        user_info = user_info_input.split(", ")
        if len(user_info) == 3:
            if user_info[0].isalpha() and validate_email(user_info[1]) and user_info[2].isdigit():
                contact = {"nom": user_info[0], "email": user_info[1], "phone": user_info[2]}
                contacts.append(contact)
                contacts_manager.save_contacts(contacts, "contacts.txt")
                display_registered_contact(contact)
            else:
                print(
                    "Please check your input. Name should only contain letters, email should be in the correct format, and phone should only contain numbers.")
        else:
            print("Please enter all three pieces of information: name, email, and phone number separated by commas.")
