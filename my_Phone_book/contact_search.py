from contacts_add import *


def search_contact():
    contacts = contacts_manager.load_contacts("contacts.txt")
    while True:
        credential = input(
            "Enter either the name or phone number or email of the contact you are looking for, or type 'exit' to quit\n")
        if credential.lower() == "exit":
            break

        found = False
        for contact in contacts:
            if credential.isdigit() and credential == contact["phone"]:
                print(f"For {credential}:\n The name is: {contact['nom']} and the email is: {contact['email']}")
                found = True
                break
            elif credential.isalpha() and credential == contact["nom"]:
                print(
                    f"For {credential}:\n The phone number is: {contact['phone']} and the email is: {contact['email']}")
                found = True
                break
            elif validate_email(credential) and credential == contact["email"]:
                print(f"For {credential}:\n The name is: {contact['nom']} and the phone number is: {contact['phone']}")
                found = True
                break

        if not found:
            print(f"Sorry! No contacts were found with this credential: {credential}")



