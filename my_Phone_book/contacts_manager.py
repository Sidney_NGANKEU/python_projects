import json


def save_contacts(contacts, filename):
    with open(filename, 'w') as file:
        json.dump(contacts, file)


def load_contacts(filename):
    try:
        with open(filename, 'r') as file:
            contacts = json.load(file)
    except FileNotFoundError:
        contacts = []
    return contacts
