from contacts_add import *


def contact_delete():
    contacts = contacts_manager.load_contacts("contacts.txt")
    while True:
        credential = input(
            "Enter the name or phone number or email of the contact you want to delete, or type 'exit' to quit\n")
        if credential.lower() == "exit":
            break

        found = False
        for contact in contacts:
            if credential.isdigit() and credential == contact["phone"]:
                found = True
                contacts.remove(contact)
                contacts_manager.save_contacts(contacts, "contacts.txt")
                print(f"{credential} has been removed")
                break
            elif credential.isalpha() and credential == contact["nom"]:
                found = True
                contacts.remove(contact)
                contacts_manager.save_contacts(contacts, "contacts.txt")
                print(f"{credential} has been removed")
                break
            elif validate_email(credential) and credential == contact["email"]:
                found = True
                contacts.remove(contact)
                contacts_manager.save_contacts(contacts, "contacts.txt")
                print(f"{credential} has been removed")
                break

        if not found:
            print(f"Sorry! No contacts were found with this credential: {credential}")
