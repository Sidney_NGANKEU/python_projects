gross_net_amount_per_hour = 11
net_amount_per_hour = 8.9


def salary(num_hours):
    return f"net salary = {net_amount_per_hour * num_hours} €\ngross salary = {gross_net_amount_per_hour * num_hours} €"


user_input = input("please indicate how many hours you worked this month so that I can calculate your salary!\n")

user_input_integer = int(user_input)

calculated_salary = salary(user_input_integer)

print(f"number of hours worked this month = {user_input_integer}")
print(calculated_salary)
