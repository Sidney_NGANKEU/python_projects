to_calculate_units = 24 * 60 * 60
name_of_unit = "seconds"


def days_to_units(num_days):
    return f"{num_days} days are {num_days * to_calculate_units} {name_of_unit}"


user_input = input("Hello!, please enter the number days and i will convert it to seconds\n")
user_input_number = int(user_input)

calculated_value = days_to_units(user_input_number)

print(calculated_value)