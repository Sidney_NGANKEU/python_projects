gross_net_amount_per_hour = 11
net_amount_per_hour = 8.9


def salary(num_hours):
    return f"net salary = {net_amount_per_hour * num_hours} €\ngross salary = {gross_net_amount_per_hour * num_hours} €"


# we don't need this anymore because .isdigit also filter negative condition

# on utiliser le try/except au lieu du isdigit(), mais il ne couvre pas les négatifs donc, on va devoir utiliser le else ci-dessous
#       (else:
#         return "you entered a negative value, so no conversion for you"  )

def validate_and_execute():
    user_input = input("please indicate how many hours you worked this month so that I can calculate your salary!\n")
    try:
        if int(user_input) > 0:
            calculated_salary = salary(int(user_input))
            print(f"number of hours worked this month = {int(user_input)}")
            print(calculated_salary)
        elif int(user_input) == 0:
            print("you entered a 0, please enter a valid positive number")
        else:
            print("you entered a negative value, so no conversion for you")
    except ValueError:
        print("your input is not a valid number. don't ruin my programme")


validate_and_execute()