import boto3
from operator import itemgetter

my_client = boto3.client('ec2', region_name="eu-central-1")
my_resource = boto3.resource('ec2', region_name="eu-central-1")

instance_id = "i-0affd83b657d45b00"

volumes = my_client.describe_volumes(      # extraction du volumeId
    Filters=[
        {
            'Name': 'attachment.instance-id',
            'Values': [instance_id]
        }
    ]
)

instance_volume = volumes['Volumes'][0]

snapshots = my_client.describe_snapshots(        # extraction du snapshot
    OwnerIds=['self'],
    Filters=[
        {
            'Name': 'volume-id',
            'Values': [instance_volume['VolumeId']]
        }
    ]
)

latest_snapshot = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)[0]   # Je Trie par la date la plus récente
print(latest_snapshot['StartTime'])

new_volume = my_client.create_volume(           # je crée un nouveau volume à partir du snapshot
    SnapshotId=latest_snapshot['SnapshotId'],
    AvailabilityZone="eu-central-1a",
    TagSpecifications=[
        {
            'ResourceType': 'volume',
            'Tags': [
                {
                    'Key': 'Name',
                    'Value': 'prod_sidney'
                }
            ]
        }
    ]
)

while True:
    vol = my_resource.Volume(new_volume['VolumeId'])    # J'attache le volume à l'instance
    print(vol.state)
    if vol.state == 'available':
        my_resource.Instance(instance_id).attach_volume(
            VolumeId=new_volume['VolumeId'],
            Device='/dev/xvdb'
        )
        break
