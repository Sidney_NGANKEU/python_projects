import boto3
from operator import itemgetter
import schedule


def clear_snapshot():
    my_client = boto3.client('ec2', region_name="eu-central-1")

    volumes = my_client.describe_volumes(         # Je filtre par le nom du service
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': ['prod_sidney']
            }
        ]
    )

    for volume in volumes['Volumes']:
        snapshots = my_client.describe_snapshots(       # Je liste tous les volumes via leurs Id
            OwnerIds=['self'],
            Filters=[
                {
                    'Name': 'volume-id',
                    'Values': [volume['VolumeId']]
                }
            ]
        )

        sorted_by_date = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)  # Je Trie par la date la plus récente

        for snap in sorted_by_date[2:]:
            response = my_client.delete_snapshot(
                SnapshotId=snap['SnapshotId']
            )
            print(response)


schedule.every(20).seconds.do(clear_snapshot)    # Scheduler automatique

while True:
    schedule.run_pending()
