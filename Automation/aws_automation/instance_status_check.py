import boto3

my_client = boto3.client("ec2", region_name="eu-central-1")

response = my_client.describe_instances()['Reservations']

for res in response:
    instance = res['Instances']
    for ins in instance:
        print(f"{ins['InstanceId']}:{ins['State']['Name']}")
