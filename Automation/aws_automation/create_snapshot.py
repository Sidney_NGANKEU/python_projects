import boto3
import schedule

# this is a snapshot of the prod EC2 instance

my_client = boto3.client('ec2', region_name="eu-central-1")


def snapshot():
    volumes = my_client.describe_volumes(   # Je filtre par le nom du service
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': ['prod_sidney']
            }
        ]
    )
    if 'Volumes' in volumes:                    # Je liste tous les volumes via leurs Id
        volumes_blocs = volumes['Volumes']
        for volume_bloc in volumes_blocs:
            vols = volume_bloc['Attachments']
            for vol in vols:
                my_client.create_snapshot(          # Je crée les snapshots
                    VolumeId=vol['VolumeId']
                )


schedule.every(20).seconds.do(snapshot)

while True:
    schedule.run_pending()