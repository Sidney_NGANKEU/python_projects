
import schedule
from operator import itemgetter

# this is a function of clear snapshot to the prod EC2 instance


def clear_snapshot():
    my_client = boto3.client('ec2', region_name="eu-central-1")
    response = my_client.describe_snapshots(
        OwnerIds=['self']
    )                                           # Je liste les snapshots que j'ai créé

    sorted_by_date = sorted(response['Snapshots'], key=itemgetter('StartTime'), reverse=True)   # Je Trie par la date la plus récente

    for snapshot in sorted_by_date[2:]:
        delete_snapshot = my_client.delete_snapshot(       # Je supprime les snapshots
        SnapshotId=snapshot['SnapshotId']
        )
        print(delete_snapshot)


schedule.every(20).seconds.do(clear_snapshot)

while True:
    schedule.run_pending()