import requests

response = requests.get("https://gitlab.com/api/v4/users/Sidney_NGANKEU/projects")
my_projects = response.json()

for project in my_projects:
    print(f"Nom du projet: {project['name']}\n Lien ssh: {project['ssh_url_to_repo']}\n Url du projet: {project['web_url']}\n")