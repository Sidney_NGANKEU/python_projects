class User:
    def __init__(self, name, password, email):
        self.user_name = name
        self.user_password = password
        self.user_email = email

    def update_user(self, user_input):
        self.user_name = user_input

    def update_password(self,password_input):
        self.user_password = password_input

    def update_email(self, email_input):
        self.user_email = email_input

    def get_user_info(self):
        print(f"username: {self.user_name}\npassword: {self.user_password}\nemail: {self.user_email}")