from openpyxl import load_workbook

wb = load_workbook(filename='inventory.xlsx')
sheet_ranges = wb['Sheet1']

product_number_per_company = {}
product_inventory_less_than_10 = {}
each_company_total_inventory_value = {}

for product_row in range(2, sheet_ranges.max_row +1):
    company_name = sheet_ranges.cell(product_row,4).value
    product_number = sheet_ranges.cell(product_row,1).value
    inventory = sheet_ranges.cell(product_row,2).value
    price = sheet_ranges.cell(product_row,3).value
    each_inventory_total_value = sheet_ranges.cell(product_row, 5)
    each_inventory_total_value.value = int(inventory) * int(price)

# calculation number of products per supplier
    if company_name in product_number_per_company:
        product_number_per_company[company_name] = product_number_per_company[company_name] + 1
    else:
        product_number_per_company[company_name] = 1


# products with inventory less than 10
    if inventory < 10:
        product_inventory_less_than_10[int(product_number)] = int(inventory)

# List each company with respective total inventory value
    if company_name in each_company_total_inventory_value:
        each_company_total_inventory_value[company_name] = each_company_total_inventory_value[company_name] + each_inventory_total_value.value
    else:
        each_company_total_inventory_value[company_name] = each_inventory_total_value.value



print(product_number_per_company)
print(product_inventory_less_than_10)
print(each_company_total_inventory_value)


wb.save("solution.xlsX")


